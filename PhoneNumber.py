import psycopg2
import datetime
from bs4 import BeautifulSoup
from urllib.request import urlopen

hostname = 'localhost'
username = 'postgres'
password = '137555'
database = 'CarPriceNullatech'


def addPhoneNum(phoneNum, brand, model, state, dealerity):
    #add data to dataBase
    dateAndTime = str(datetime.datetime.now()).split(" ")
    #current date and time will save in dataBase
    date = dateAndTime[0]
    time = dateAndTime[1][0:8]
    global hostname, username, password, database
    conn = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = conn.cursor()
    if dealerity:
        sql = "Insert into PhoneNum (Phnum, brand, model, state, date, time, isDealer)" + \
              "\nvalues ( \'" + str(
            phoneNum) + "\' , \'" + brand + "\',\'" + model + "\', \'" + state + "\', \'" + date + "\', \'" + \
            time + "\', True)"
    else:
        sql = "Insert into PhoneNum (Phnum, brand, model, state, date, time, isDealer)" + \
              "\nvalues ( \'" + str(
            phoneNum) + "\' , \'" + brand + "\',\'" + model + "\', \'" + state + "\', \'" + date + "\', \'" + \
              time + "\', False)"
    cur.execute(sql)
    conn.commit()


def crawlPhoneNum(url):
    open_site_url = urlopen(url).read()
    soup = BeautifulSoup(open_site_url, "html.parser")
    State = "Unvalid"
    for item in soup.find_all("div", {"class": "inforight"}):
        #Crawl State from page
        text = item.text
        data = text.split("\n")
        State = data[49].replace(" ", "")

    #get Brand and model from url address
    EnBrand, EnModel = splitURL(url)

    #Collect number from html
    htmlString = str(soup.select("#wrapper > script")[1])
    varIndex = htmlString.index("var phoneNo = ")
    semiColonIndex = htmlString.index(";", varIndex)
    secondSemiColonIndex = htmlString.index(";", semiColonIndex + 1)
    Numbers = htmlString[varIndex:secondSemiColonIndex].split("\'")
    phoneNumbers = (Numbers[1]).replace(" ", "").replace("،",",").split(",")
    areaCode = Numbers[3].replace(" ", "").replace(")", "").replace("(", "")
    allNumbers = []
    for pNum in phoneNumbers:
        allNumbers.append(areaCode + pNum)
    return allNumbers, EnBrand, EnModel, State


def splitURL(url):
    data = url.split("/")
    detail = data[-1]
    splitedDetail = detail.split("-")
    lenth = len(splitedDetail)
    BrandModel = splitedDetail[1]
    for i in range(2, lenth - 2):
        BrandModel += "-" + splitedDetail[i]
    # Data is List of Brands
    Brands = open("PhoneBrandsList.txt", "r")
    for brand in Brands:
        brand = brand.replace("\n", "")
        if BrandModel.__contains__(brand):
            Brand = brand.replace("-", "_")
            brandLen = len(Brand)
            Model = BrandModel[brandLen + 1:len(BrandModel)].replace("-", "_")
            return Brand, Model


def findBrandLists():
    #List of Only Brands for collecting model from url with it
    site_url = 'https://bama.ir/'
    open_site_url = urlopen(site_url).read()
    soup = BeautifulSoup(open_site_url, "html.parser")

    # file is List of Brands
    file = open("PhoneBrandsList.txt", "w")
    file.write("")
    file.close()
    Data = []

    for link in soup.find_all('a', href=True):
        text = link['href']
        textLen = len(text)
        brand = text[5:textLen]
        if text[0:4] == "/car" and text[5:8] != "all" and text.count("/") == 2 and not Data.__contains__(
                        brand + "\n"):
            Data.append(brand + "\n")

    file = open("PhoneBrandsList.txt", "a")
    for data in Data:
        file.write(data)
    file.close()


def findModelsLinks():
    #Collect all car Models
    site_url = 'https://bama.ir/'
    open_site_url = urlopen(site_url).read()
    soup = BeautifulSoup(open_site_url, "html.parser")

    # Data is List of Brands
    Data = []
    for link in soup.find_all('a', href=True):
        text = link['href']
        if text[0:4] == "/car" and text[5:8] != "all" and text.count("/") > 2 and not Data.__contains__(
                                "https://bama.ir" + text + "\n"):
            Data.append("https://bama.ir" + text + "\n")

    file = open("PhoneBrands.txt", "w")
    file.write("")
    file.close()
    file = open("PhoneBrands.txt", "a")
    for data in Data:
        file.write(data)
    file.close()


def findElementsLinks(address):
    #Collect All car Links in bama site.
    i = 1
    while True:
        # print(str(i)+ ")---------------")
        site_url = address + "?page=" + str(i)
        try:
            open_site_url = urlopen(site_url).read()
        except TimeoutError:
            i = i - 1
            continue
        soup = BeautifulSoup(open_site_url, "html.parser")
        ctr = 1
        flag = False
        for link in soup.find_all('a', href=True):
            text = link['href']
            if text[5:12] == "details" and flag:
                ctr += 1
                file = open("PhoneLinks.txt", "a")
                file.write("https://bama.ir" + text + "\n")
                file.close()
            flag = not flag
        # print("CTR is: "+ str(ctr))
        if ctr < 12:
            return
        for tt in soup.select("#content > div.leftpanel > div.paging-bottom-div.hidden-xs > h4"):
            text = tt.text
            splited = text.split(" ")
            if splited[5] == splited[7]:
                return
        i += 1


def findLimitElementsLinks(address):
    #collecting links only with first pages of each model.
    maxPage = 3
    for i in range(1, maxPage + 1):
        # print(str(i)+ ")---------------")
        site_url = address + "?page=" + str(i)
        try:
            open_site_url = urlopen(site_url).read()
        except TimeoutError:
            i = i - 1
            continue
        soup = BeautifulSoup(open_site_url, "html.parser")
        ctr = 1
        flag = False
        for link in soup.find_all('a', href=True):
            text = link['href']
            if text[5:12] == "details" and flag:
                ctr += 1
                file = open("PhoneLimitedLinks.txt", "a")
                file.write("https://bama.ir" + text + "\n")
                file.close()
            flag = not flag
        # print("CTR is: "+ str(ctr))
        if ctr < 12:
            return
        for tt in soup.select("#content > div.leftpanel > div.paging-bottom-div.hidden-xs > h4"):
            text = tt.text
            splited = text.split(" ")
            if splited[5] == splited[7]:
                return

def isDealer(url):
    open_site_url = urlopen(url).read()
    soup = BeautifulSoup(open_site_url, "html.parser")
    for dealer in soup.select("#content > div.prd-detail.ad-detail-maindiv > div.top-coprporationdetail.hidden-xs > div.right-corporation-div > h3 > a"):
        if str(dealer['href']) == "":
            return False
        else:
            return True
    return False
def getAll():
    # file = open("PhoneLimitedLinks.txt", "w")
    # file.write("")
    # file.close()
    # file = open("PhoneLinks.txt", "w")
    # file.write("")
    # file.close()
    # findBrandLists()
    # findModelsLinks()
    # reader = open("PhoneBrands.txt", "r")
    # for line in reader:
    #     # find all links
    #     # findElementsLinks(line.replace("\n",""))
    #
    #     # find only 3 first page
    #     findLimitElementsLinks(line.replace("\n", ""))
    # reader.close()

    reader = open("PhoneLimitedLinks.txt", "r")
    for line in reader:
        numbers, EnBrand, EndModel ,State = crawlPhoneNum(line.replace("\n",""))
        deality = isDealer(line.replace("\n",""))
        for phnumber in numbers:
            addPhoneNum(phnumber,EnBrand,EndModel,State, deality)

    reader.close()


getAll()
